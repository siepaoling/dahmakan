
public interface DistanceService {
/**
* Returns the duration in seconds to get from Delivery `a` to Delivery `b`.
* The following always holds: getDistance(a, b) == getDistance(b, a)
*/
int getDurationInSeconds(Delivery a, Delivery b);
}
