import java.util.Arrays;
import java.util.stream.IntStream;

public class Test {

	public static void main(String[] args) {
		/*
		 * TEST ONE
		 */
		String str="thisContainsFourWords";
		//String str="ThisContainsFourWords";
		System.out.println("TestOne Result = "+TestOne(str));
		
		
		/*
		 * TEST TWO
		 */
		//sample input data
		int[] route = { 1, 4, 1, 3, 1 };
		//int[] route = { 1, 4, 1, 3};
		//int[] route = { 1, 1};
		//int[] route = { 1, 1, 1};
		//int[] route = { 1, 1, 1, 1, 1, 1};
		System.out.println("TestTwo Result = "+TestTwo(route));

		/*
		 * TEST THREE
		 */
		
		BackupServiceImpl bService=new BackupServiceImpl();
		//bService.findInsertion(deliveryToInsert, activeRoutes);
		
	}
	
	public static Long TestOne(String camelCaseString){
		//convert the first char to upper case
		String str = camelCaseString.substring(0, 1).toUpperCase() + camelCaseString.substring(1);
		
		//count upper case chars
		long camelCaseCount = str.chars().filter(p -> p >=65 && p<=90).count();
		
		return camelCaseCount;
	}
	
	public static Integer TestTwo(int[] route) {
		
		for (int i=0;i<route.length;i++){
			
			int firstSum = IntStream.of (Arrays.copyOfRange(route,0,i+1)).sum();
			int secondSum = IntStream.of (Arrays.copyOfRange(route,i+1,route.length)).sum();
			
			//debug
			//System.out.println("firstSum ="+firstSum);
			//System.out.println("secondSum ="+secondSum);
			
			if (firstSum == secondSum)
				return i;
		}
		
		return null;
	
	}

}
