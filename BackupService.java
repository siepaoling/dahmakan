import java.util.List;


public interface BackupService {
	/**
	* Returns an insertion for `deliveryToInsert` into any of the provided `activeRoutes` with an index
	*/
	Insertion findInsertion(Delivery deliveryToInsert, List<Route> activeRoutes);
}
