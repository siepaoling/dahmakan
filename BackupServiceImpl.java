import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;


public class BackupServiceImpl implements BackupService{
	
	@Override
	public Insertion findInsertion(Delivery deliveryToInsert, List<Route> activeRoutes) {
		DistanceServiceImpl checkDistance= new DistanceServiceImpl();
		
		TreeMap<Integer,Route> routeDeliveryTime=new TreeMap<Integer,Route>();
		HashMap<Route,Integer> routeDeliveryTimeIndex=new HashMap<Route,Integer>();
		
		/*
		 * Find total shortest delivery time in each route
		 */
		for (Route r:activeRoutes){
			
			Integer totalRouteDeliveryTime=0;
			
			for (int i=0;i<r.getDeliveries().size();i++){
				
				if ((i+1)<r.getDeliveries().size())
					totalRouteDeliveryTime+=checkDistance.getDurationInSeconds(r.getDeliveries().get(i), r.getDeliveries().get(i+1));
				
			}
			
			Integer totalShortestDeliveryTimeAfterInsert=Integer.MAX_VALUE;
			Integer indexShortestRoute=0;
			
			for (int i=0;i<r.getDeliveries().size();i++){
				int additionalInsertionTime=0;
				
				
				if ((i+1)<r.getDeliveries().size()){
				
					additionalInsertionTime+=checkDistance.getDurationInSeconds(r.getDeliveries().get(i), deliveryToInsert);
					additionalInsertionTime+=checkDistance.getDurationInSeconds(deliveryToInsert,r.getDeliveries().get(i+1));
					
					int totalDeliveryTimeAfterInsert=totalRouteDeliveryTime-checkDistance.getDurationInSeconds(r.getDeliveries().get(i), r.getDeliveries().get(i+1))+additionalInsertionTime;
					
					if (totalDeliveryTimeAfterInsert<totalShortestDeliveryTimeAfterInsert){
						totalShortestDeliveryTimeAfterInsert=totalDeliveryTimeAfterInsert;
						indexShortestRoute=i;
					}
					
				}else{

					additionalInsertionTime+=checkDistance.getDurationInSeconds(r.getDeliveries().get(i), deliveryToInsert);
					
					int totalDeliveryTimeAfterInsert=totalRouteDeliveryTime+additionalInsertionTime;
					
					if (totalDeliveryTimeAfterInsert<totalShortestDeliveryTimeAfterInsert){
						totalShortestDeliveryTimeAfterInsert=totalDeliveryTimeAfterInsert;
						indexShortestRoute=i;
					}
					
				}
			}
			
			
			routeDeliveryTime.put(totalShortestDeliveryTimeAfterInsert,r);
			routeDeliveryTimeIndex.put(r, indexShortestRoute);
	
		}
		
		
		/*
		 * Return the shortest delivery time from all routes
		 */
		Route shortestRoute=routeDeliveryTime.get(routeDeliveryTime.firstKey());
		Integer shortestIndex=routeDeliveryTimeIndex.get(shortestRoute);
		
		return new Insertion(shortestRoute,deliveryToInsert,shortestIndex);
	}

}
