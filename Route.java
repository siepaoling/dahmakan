import java.util.List;


public interface Route {
	List<Delivery> getDeliveries();
}
